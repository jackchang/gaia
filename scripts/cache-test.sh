#!/bin/bash

echo PUT CACHE
curl -XPUT http://127.0.0.1:8001/.test.cache/foo -d bar
echo

echo GET CACHE
curl -XGET http://127.0.0.1:8001/.test.cache/foo
echo

echo POST CACHE
curl -v -XPOST http://127.0.0.1:8001/.test.cache -d \
"foo1 bar1
foo2 bar2
foo3 bar3
foo1 bar1.1
"
echo

echo GET CACHE
curl -XGET http://127.0.0.1:8001/.test.cache/foo1
curl -XGET http://127.0.0.1:8001/.test.cache/foo2
curl -XGET http://127.0.0.1:8001/.test.cache/foo3
echo
