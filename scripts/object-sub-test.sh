#!/bin/bash

source color.sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

section "delete data"
${DIR}/object/delete-test.sh
echo

section "get data - should be all empty"
${DIR}/object/get-test.sh
echo

section "upload schema"
${DIR}/object/upload-schema-test.sh
echo

    section "auto table test"
    ${DIR}/object/auto-table-test.sh
    echo
    sleep 3

#for i in {1..64}
#do
    section "upload data"
    ${DIR}/object/put-test.sh
    echo
    sleep 3

    exit

    section "get data"
    ${DIR}/object/get-test.sh
    echo
    sleep 1

    section "query data"
    ${DIR}/object/query-test.sh
    echo
    sleep 1
#done
