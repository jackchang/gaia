#!/bin/bash

source color.sh

## shadow schema - use single entry point
## to generate two consolidated records
message "upload schema [Vendor.RoadMio.Trip]"
curl -XPATCH http://127.0.0.1:8001/Vendor.RoadMio.Trip -d \
"module.exports = (core) => core('Vendor.RoadMio.Trip')
.shadowClass(true)
.require('vehicleName')
.validate('driver-id-exist', (obj) => {
    return !!obj.driverId;
})
.onReject((objs) => {
    objs.forEach((obj) => {
        logger.info('    ' + obj[1] + ' - reject ' + JSON.stringify(obj[0]));
    });
})
.onPut((objs) => {
    return core('vendor.roadmio.trip.byvehicle').put(objs);
})
.onPut((objs) => {
    return core('vendor.roadmio.trip.bydriver').put(objs);
})
.id((obj) => obj.tripId);"  2> /dev/null | json_pp
echo


## multi-value record - use name + year + month + dayOfMonth
## to consolidate multiple values into single key
## for query speed acceleration (one grouped object per day)
message "upload schema [vendor.roadmio.trip.byvehicle]"
curl -XPATCH http://127.0.0.1:8001/Vendor.RoadMio.Trip.ByVehicle -d \
"module.exports = (core) => core('vendor.roadmio.trip.byvehicle')
.id((obj) => obj.vehicleName + '-' + require('sugar').Date.format(new Date(obj.fromEpochTime * 1000), '{year}-{MM}-{dd}'))
.multiValue(true)
.onPut((objs) => logger.info('....put ' + objs.length + ' vendor.roadmio.trip.byvehicle'))
.merger((objs) => objs)
.watch('vehicleName')
.watch('driverId')
.watch('tripId');"  2> /dev/null | json_pp
echo

## multi-value record - use name + year + month + dayOfMonth
## to consolidate multiple values into single key
## for query speed acceleration (one grouped object per day)
message "upload schema [vendor.roadmio.trip.bydriver]"
curl -XPATCH http://127.0.0.1:8001/Vendor.RoadMio.Trip.ByDriver -d \
"module.exports = (core) => core('vendor.roadmio.trip.bydriver')
.id((obj) => obj.driverId + '-' + require('sugar').Date.format(new Date(obj.fromEpochTime * 1000), '{year}-{MM}-{dd}'))
.multiValue(true)
.watch('vehicleName')
.watch('driverId')
.watch('tripId');"  2> /dev/null | json_pp
echo

## test only...
message "upload schema [namespace.schematest]"
curl -XPATCH http://127.0.0.1:8001/namespace.schematest -d ''  2> /dev/null | json_pp
# curl -XPATCH http://127.0.0.1:8001/namespace.schematest -d \
# "module.exports = (core) => core('Namespace.SchemaTest')
# .id((obj) => obj.objectId)
# .multiValue(true)
# .searchOrder(['int', 'str', 'foobar'])
# .watch('key')
# .watch('lastCommit');"
echo



message "upload schema [test.data]"
curl -XPATCH http://127.0.0.1:8001/test.data -d \
"module.exports = (core) => core('test.data')
    .id('oid')
    .watch('index1');
" 2> /dev/null | json_pp
echo

message "upload schema [test.cachetest]"
curl -XPATCH http://127.0.0.1:8001/test.cachetest -d \
"module.exports = (core) => core('test.cachetest')
    .shadowClass(true)
    .onPut((objs) => objs.forEach((obj) => {
        core('test.data').retain(obj.oid)
            .then((d) => {
            })
    }));
" 2> /dev/null | json_pp
echo

message "upload schema [test.autotable]"
curl -XPATCH http://127.0.0.1:8001/test.autotable -d \
"module.exports = (core) => core('test.autotable')
    .watch('str1')
    .watch('str2')
    .watch('int1')
    .watch('aint')
    .autoBuild()
" 2> /dev/null | json_pp
echo

message "upload schema [test.autotable2]"
curl -XPATCH http://127.0.0.1:8001/test.autotable2 -d \
"module.exports = (core) => core('test.autotable2')
    .watch('str1')
    .autoBuild()
" 2> /dev/null | json_pp
echo


message "upload schema [Vendor.RoadMio.PacketHandlers.V2]"
curl -XPATCH http://127.0.0.1:8001/Vendor.RoadMio.PacketHandlers.V2 -d \
"const newTrip = (devId) => {
    let now = Date.now();
    return {
        dev: devId,
        start: now,
        miles: 0,
        lastReport: now,
        reports: 0,
    };
};

module.exports = (core) => core('roadmio.packet.v2')
    .shadowClass(true)
    .onPut((objs) => {
        objs.forEach((obj) => {
            let devId = obj.devId;
            if (!devId) { return; }

            core('roadmio.currentTrip').recall(devId, 15)
                .then((t) => t || newTrip(devId))
                .then((t) => {
                    t.miles += obj.miles;
                    t.reports += 1;
                    t.lastReport = Date.now();
                    console.info(t);
                    core('roadmio.currentTrip').cache(devId, t);
                });
        });
    });" 2> /dev/null | json_pp
echo


message "trigger schema reload"
curl -XPATCH http://127.0.0.1:8001/*/* -d "" 2> /dev/null | json_pp
echo

sleep 3

exit 0






