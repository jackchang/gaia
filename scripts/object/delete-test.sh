#!/bin/bash

curl -XDELETE http://127.0.0.1:8001/vendor.roadmio.trip.byvehicle/vehicle1-2017-03-06
curl -XDELETE http://127.0.0.1:8001/vendor.roadmio.trip.byvehicle/vehicle2-2017-03-06
curl -XDELETE http://127.0.0.1:8001/vendor.roadmio.trip.byvehicle/vehicle3-2017-03-06
curl -XDELETE http://127.0.0.1:8001/vendor.roadmio.trip.byvehicle/vehicle4-2017-03-06
curl -XDELETE http://127.0.0.1:8001/vendor.roadmio.trip.byvehicle/undefined
echo

curl -XDELETE http://127.0.0.1:8001/vendor.roadmio.trip.bydriver/driver1-2017-03-06
curl -XDELETE http://127.0.0.1:8001/vendor.roadmio.trip.bydriver/driver2-2017-03-06
curl -XDELETE http://127.0.0.1:8001/vendor.roadmio.trip.bydriver/driver3-2017-03-06
curl -XDELETE http://127.0.0.1:8001/vendor.roadmio.trip.bydriver/driver4-2017-03-06
curl -XDELETE http://127.0.0.1:8001/vendor.roadmio.trip.bydriver/undefined
echo
