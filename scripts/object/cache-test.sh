#!/bin/bash

curl -XPATCH http://127.0.0.1:8001/test.data -d \
"module.exports = (core) => core('test.data')
    .id('oid')
    .watch('index1')
    .watch('n1');
"

curl -XPATCH http://127.0.0.1:8001/test.cachetest -d \
"module.exports = (core) => core('test.cachetest')
    .shadowClass(true)
    .onPut((objs) => objs.forEach((obj) => {
        core('test.data').retain(obj, 10)
            .then((d) => {
                console.info(JSON.stringify(obj) + ' = ' + JSON.stringify(d));
            })
            .catch((e) => {
                console.info(JSON.stringify(obj) + ' ' + e);
            });
    }));
"


echo trigger schema reload
curl -XPATCH http://127.0.0.1:8001/*/* -d ""
echo


curl -XPUT http://127.0.0.1:8001/test.data -d \
'[{"oid": "object1", "index1": "value1", "n1": 10}]'

curl -XPUT http://127.0.0.1:8001/test.cachetest -d \
'["object1","object2",{"index1": "value1"},{"index1": "value2"},{"n1": 10},{"n1": [0,8]},{"n1": [8,10]}]'
