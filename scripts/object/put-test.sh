#!/bin/bash

source color.sh

# trigger update
message "trigger update"
for i in {1..10}
do
    curl -XPUT http://127.0.0.1:8001/roadmio.packet.v2 -d \
    '[{"devId":"foobar", "miles": 0.7}]' 2> /dev/null | json_pp
    echo
    sleep 1
done

# put normal data
message "put normal data"
for i in {1..10}
do
    curl -XPUT http://127.0.0.1:8001/Vendor.RoadMio.Trip -d \
    '[{"vehicleName":"vehicle1","driverId":"driver1","fromEpochTime":1488775387}]' 2> /dev/null | json_pp
    echo
done

# put invalid data
message "put invalid"
for i in {1..5}
do
    curl -XPUT http://127.0.0.1:8001/Vendor.RoadMio.Trip -d \
    '[
    {                         "driverId":"driver1","fromEpochTime":1488775387},
    {"vehicleName":"vehicle1",                     "fromEpochTime":1488775387}
    ]' 2> /dev/null | json_pp
    echo
done

sleep 3

##{"vehicleName":"vehicle1","driverId":"driver4","fromEpochTime":1488775387}
##{"vehicleName":"vehicle2","driverId":"driver1","fromEpochTime":1488775387},
##{"vehicleName":"vehicle2","driverId":"driver2","fromEpochTime":1488775387},
##{"vehicleName":"vehicle3","driverId":"driver3","fromEpochTime":1488775387},
##{"vehicleName":"vehicle4","driverId":"driver4","fromEpochTime":1488775387}
