#!/bin/bash

source color.sh

message "get trip of vehicle1 @ 2017-03-06"
curl -XGET http://127.0.0.1:8001/vendor.roadmio.trip.byvehicle/vehicle1-2017-03-06
echo

message "get trip of driver1 @ 2017-03-06"
curl -XGET http://127.0.0.1:8001/vendor.roadmio.trip.bydriver/driver1-2017-03-06
echo

message "search trip of driver1 @ 2017-03-06"
curl -XSEARCH http://127.0.0.1:8001/vendor.roadmio.trip.bydriver?driverId=driver1
echo

