#!/bin/bash

source color.sh

message "put test.autotahble"
curl -XPUT http://127.0.0.1:8001/test.autotable -d \
'[{"str1":"str1v","int1":1377,"aint":[1,3,5,7]}]' 2> /dev/null | json_pp
curl -XPUT http://127.0.0.1:8001/test.autotable -d \
'[{"str1":"str1v","int1":2377,"aint":[1,3,5,7]}]' 2> /dev/null | json_pp
curl -XPUT http://127.0.0.1:8001/test.autotable -d \
'[{"str1":"str1v","int1":3377,"aint":[1,3,5,7]}]' 2> /dev/null | json_pp
echo

message "get test.autotable - should contains 3 entries"
curl "http://127.0.0.1:8001/test.autotable?str1=str1v" 2> /dev/null | json_pp
echo

message "get test.autotable - should contains 1 entries"
curl "http://127.0.0.1:8001/test.autotable?str1=str1v&int1=1377" 2> /dev/null | json_pp
echo

message "get test.autotable with force key - should contains 1 entries"
curl "http://127.0.0.1:8001/test.autotable/str1/str1v?int1=1377" 2> /dev/null | json_pp
echo

message "get test.autotable - should contains 2 entries"
curl "http://127.0.0.1:8001/test.autotable?str1=str1v&int1=1377&int1=2500" 2> /dev/null | json_pp
echo

message "get test.autotable - should contains 1 entries"
curl "http://127.0.0.1:8001/test.autotable?str1=str1v&int1=1800&int1=2500" 2> /dev/null | json_pp
echo
