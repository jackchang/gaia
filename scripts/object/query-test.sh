# #!/bin/bash

source color.sh

message "query trip by vehicle name"
curl -XGET http://127.0.0.1:8001/vendor.roadmio.trip.byvehicle?vehicleName=vehicle1 2> /dev/null | json_pp
echo

# echo query trip by driver id
# curl -XGET http://127.0.0.1:8001/vendor.roadmio.trip.bydriver?driverId=driver1
# echo
