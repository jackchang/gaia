#!/bin/bash

echo INSERT
curl -XPUT http://127.0.0.1:8001/.test.cache/foo -d \
"{\"a\":[0,1,2,3,4,5,6,7,8,9],\"o\":{\"p\":13},\"s\":\"bar\"}"
echo

echo GET foo
curl http://127.0.0.1:8001/.test.cache/foo
echo

echo GET foo.o.p - should be 13
curl http://127.0.0.1:8001/.test.cache/foo?key=o.p
echo

echo GET foo.a.2 - should be 9
curl http://127.0.0.1:8001/.test.cache/foo?key=a.2
echo

echo GET foo.a.-2 - should be 1
curl http://127.0.0.1:8001/.test.cache/foo?key=a.-2
echo

echo GET foo.s - should be bar
curl http://127.0.0.1:8001/.test.cache/foo?key=s
echo

echo GET foo/o/p - should be 13
curl http://127.0.0.1:8001/.test.cache/foo/o/p
echo

echo GET foo/a/2 - should be 2
curl http://127.0.0.1:8001/.test.cache/foo/a/2
echo

echo GET foo/a/-2 - should be 8
curl http://127.0.0.1:8001/.test.cache/foo/a/-2
echo

echo GET foo/a/-2,-1 - should be 8,9
curl http://127.0.0.1:8001/.test.cache/foo/a/-2,-1
echo

echo GET foo/a/-1,-2 - should be 8,9
curl http://127.0.0.1:8001/.test.cache/foo/a/-1,-2
echo

echo GET foo/a/0,2 - should be 0,1,2
curl http://127.0.0.1:8001/.test.cache/foo/a/0,2
echo

echo GET foo/a/2,0 - should be 0,1,2
curl http://127.0.0.1:8001/.test.cache/foo/a/2,0
echo

echo GET foo/a/length - should be 10
curl http://127.0.0.1:8001/.test.cache/foo/a/length
echo

echo GET foo/s - should be bar
curl http://127.0.0.1:8001/.test.cache/foo/s
echo