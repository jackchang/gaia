#!/bin/bash

echo upload api [test-api]
curl -XPATCH http://127.0.0.1:8001/apis/test-api -d \
"
const path = require('path');
module.exports = {
    get: (req, res) => {
        res.type('text/plain')
            .status(200)
            .send('sub url=[' + req.url + ']')
            .end();
    }
};
"
echo
echo

echo reload api
curl -XPATCH http://127.0.0.1:8001/apis/ -d ""
echo