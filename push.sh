#!/bin/sh

source color.sh
foreground ${Yellow}

section "create tarball"
rm -rf Files
mkdir Files
cd src
cp -R $(ls . | grep -v node_modules) ../Files
cd ..
tar -czf GAIA Files
rm -rf Files

section "upload tarball"
scp -P 10202 GAIA dpm-admin@dpm-bin.mobric.com:~/
rm -rf GAIA

section "repack remote package [gaia]"
ssh -tq -p 10202 dpm-admin@dpm-bin.mobric.com "sudo bash -c \"cd /home/dpm-admin; ./scripts/repack.sh gaia\""

reset_console

