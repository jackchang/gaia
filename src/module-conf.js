const fs = require('fs');
const path = require('path');
const url = require('url');
const jsons = require('JSONStream');
const readline = require('readline');
const vm = require('vm');
const serdes = require('node-serialize');
const sugar = require('sugar');
const https = require('https');
const http = require('http');
const express = require('express');
const ws = require('ws').Server;

const riak = require(path.resolve('lib', 'riak.js'));
const restful = require(path.resolve('lib', 'restful.js'));
const util = require(path.resolve('lib', 'http-util.js'));
const logger = require(path.resolve('lib', 'logger.js'));
const timer = require(path.resolve('lib', 'timer.js'));

const config = require(path.resolve('config.js'));
const bucket = '::config';

const parse = (str) => {
    try { return JSON.parse(str); }
    catch (e) { return str; }
};

module.exports = new Promise((ok) => riak.onload(() => {
    riak.list(bucket).then((l) => {
            let i = 0;
            let get = () => {
                if (i === l.length) { 
                    return ok(); 
                }

                let k = l[i++];
                let keys = k.split('.');
                riak.type('application/octet-stream').get(bucket, k).then((v) => {
                    let o = config;
                    for (let j = 1; j < keys.length; ++j) {
                        let ki = keys[j - 1];
                        if (!o[ki]) { o[ki] = {}; }
                        o = o[ki];
                    }
                    let p = keys[keys.length - 1];
                    o[keys[keys.length - 1]] = parse(v.toString());
                }).then(get);
            };
            get();
        });
}));
