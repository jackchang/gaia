const fs = require('fs');
const path = require('path');
const url = require('url');
const jsons = require('JSONStream');
const readline = require('readline');
const vm = require('vm');
const serdes = require('node-serialize');
const sugar = require('sugar');
const https = require('https');
const http = require('http');
const express = require('express');
const ws = require('ws').Server;

const riak = require(path.resolve('lib', 'riak.js'));
const restful = require(path.resolve('lib', 'restful.js'));
const util = require(path.resolve('lib', 'http-util.js'));
const logger = require(path.resolve('lib', 'logger.js'));
const timer = require(path.resolve('lib', 'timer.js'));
const cluster = require(path.resolve('lib', 'cluster.js'));
const schema = require(path.resolve('lib', 'schema-core.js'))

const config = require(path.resolve('config.js'));

const loadJSON = (ifs, single) => {
    const l = [];
    return new Promise((ok, ng) => {
        ifs.pipe(jsons.parse({recurse: true}))
            .on('data', (d) => { l.push(d); })
            .on('end', () => { ok((single) ? l[0] : l); })
            .on('error', (e) => { ng(e); });
    });
};

const types = schema.types;
cluster.on('module.obj', 'schema.reload', (msg) => {
    schema.reload();
});

riak.onload(() => {
    schema.reload().catch((e) => {
        console.error(e.record);
        console.info(e.message);
    });
});

module.exports = (util) => express.Router()
    .patch('*', (req, res) => {
        let uri = url.parse(req.baseUrl, true);
        let tks = (uri.pathname || '').split('/').filter((v) => !!v);
        if (tks.length !== 1) {
            cluster.broadcast({ 
                process: process.pid, 
                sender: 'module.obj', 
                type: 'schema.reload',
            }, true);
            return schema.reload()
                .then(() => {
                    let t = {};
                    Object.keys(types).forEach((k) => {
                        let def = types[k];
                        t[k] = {
                            shadowClass: def.shadowClass,
                            idgen: !!def.idgen,
                            indexer: Object.keys(def.indexer),
                            searchOrder: def.searchOrder,
                            onput: def.onPut.length,
                            merger: !!def.merger,
                            validators: Object.keys(def.validators)
                        };
                    });
                    util.finish(res, 200, '', t);
                })
                .catch((e) => {
                    util.finish(res, 400, '', e);
                });
        }

        util.all(req).then((c) => {
            if (c.length === 0) {
                return riak.delete('::sys', tks[0])
                    .then(() => util.finish(res, 200))
                    .catch((e) => util.finish(res, 404, e)); 
            }

            riak.type('application/javascript')
                .index({ type: 'schema' })
                .put('::sys', tks[0], c)
                .then(() => util.finish(res, 200))
                .catch((e) => util.finish(res, 404, e)); 
        });
    })
    .delete('*', (req, res) => {
        let uri = url.parse(req.baseUrl, true);
        let tks = (uri.pathname || '').split('/').filter((v) => !!v);
        if (tks.length === 0) {
            return util.finish(res, 404, 'invalid object path');
        }

        let bkt = tks[0];
        let kvs = url.parse(req.url, true).query || {};
        let kcount = Object.keys(kvs).length
        if (tks.length === 1 && kcount !== 1) {
            return util.finish(res, 404, 'invalid object query');
        }

        let key = Object.keys(kvs)[0];
        ((tks.length !== 1) ? Promise.resolve([tks[1]]) : schema.find(bkt, key, kvs[key])).then((l) => {
            let idx = 0;
            let del = () => {
                if (idx === l.length) { return util.finish(res, 200); }
                riak.delete(bkt, l[idx++])
                    .then(del)
                    .catch((e) => util.finish(res, 404, e));
            };
            del();
        });
    })
    .search('*', (req, res) => {
console.info(req.url);
        let uri = url.parse(req.baseUrl, true);
        let tks = (uri.pathname || '').split('/').filter((v) => !!v);
        if (tks.length === 0) {
            return util.finish(res, 200);
        }

        let bkt = tks[0];
        let kvs = url.parse(req.url, true).query || {};
        let kcount = Object.keys(kvs).length
        if (kcount !== 1) {
            return util.finish(res, 400, 'invalid query');                
        }

        let key = Object.keys(kvs)[0];
        schema.find(bkt, key, kvs[key])
            .then((l) => util.finish(res, 200, '', l))
            .catch((e) => util.finish(res, 404, e));
    })
    .get('*', (req, res) => {
        const uri = url.parse(req.baseUrl, true);
        const tks = (uri.pathname || '').split('/').filter((v) => !!v);
        if (tks.length === 0) {
            return util.finish(res, 200);
        }

        const bkt = tks[0];
        const def = types[bkt] || schema.new(bkt, 2);
        if (tks.length === 2) {
            return def.getObjects(tks[1])
                .then((a) => util.finish(res, 200, '', a))
                .catch((e) => util.finish(res, 404, e));
        }

        const idx = url.parse(req.url, true).query || {};
        if (tks.length === 3) {
            idx[tks[1]] = tks[2];
        }
        else if (tks.length === 4) {
            idx[tks[1]] = [tks[2], tks[3]];
        }

        if (Object.keys(idx).length === 0) {
            return util.finish(res, 200);
        }

        return def.getObjects(idx, (tks.length > 1) ? tks[1] : null)
            .then((a) => util.finish(res, 200, '', a))
            .catch((e) => util.finish(res, 404, e));
    })
    .put('*', (req, res) => {
        const uri = require('url').parse(req.baseUrl, true);
        const tks = (uri.pathname || '').split('/').filter((v) => !!v);
        const bkt = tks[0];
        if (!bkt) {
            return util.finish(res, 404, 'invalid object type ' + bkt);
        }

        const def = types[bkt] || schema.new(bkt, 3);
        if ((tks.length === 1) && (!def.idgen) && (!def.shadowClass)) {
            return util.finish(res, 404, 'invalid object type ' + bkt, def);
        }

        const oid = tks[1];
        const idx = url.parse(req.url, true).query;
        loadJSON(req, true)
            .then((objs) => def.putObjects(oid, idx, objs))
            .then((r) => util.finish(res, r[0], r[1]))
            .catch((e) => util.finish(res, 404, e.toString()));
    });