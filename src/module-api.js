const fs = require('fs');
const path = require('path');
const url = require('url');
const jsons = require('JSONStream');
const readline = require('readline');
const vm = require('vm');

const https = require('https');
const http = require('http');
const express = require('express');
const ws = require('ws').Server;
const riak = require(path.resolve('lib', 'riak.js'));

const logger = require(path.resolve('lib', 'logger.js'));
const timer = require(path.resolve('lib', 'timer.js'));
const config = require(path.resolve('config.js'));

const apiRoot = path.resolve(config.apis.path);
const modules = {};
const api = (util) => { 
    let remove = (name) => {
        let pth = '/' + name;
        let idx = inst.stack.findIndex((o) => o.path === pth && o.name === 'router');
        if (idx !== -1) {
            inst.stack.splice(idx, 1);
        }
        delete modules[name];
    };
    let inst = express.Router()
        .patch('*', (req, res) => {
            const name = url.parse(req.url).pathname.substring(1);
            if (!name) {
                Object.keys(modules).forEach(remove);
                return util.finish(res, 200, 'api cache cleaned');
            }

            util.all(req).then((c) => {
                    if (!c) {
                        return Promise.reject('empty script');
                    }
                    return riak.type('application/javascript')
                        .index({ type: 'api' })
                        .put('::api', name, c);
                })
                .then(() => util.finish(res, 200))
                .catch((e) => util.finish(res, 404, e));
            remove(name);
        })
        .use((req, res, next) => {
            const name = url.parse(req.url).pathname.substring(1);
            if (modules[name]) {
                return next();
            }

            riak.type('*').get('::api', name).then((scr) => {
                let ctx = {module:{},require:require};
                try {
                    let script = new vm.Script(scr.toString());
                    script.runInNewContext(ctx);
                }
                catch (e) {
                    return Promise.reject(e);
                }

                let methods = modules[name] = ctx.module.exports;
                let sub = express.Router();
                Object.keys(methods).forEach((method) => {
                    let cb = methods[method];
                    sub[method]('/', (req, res) => {
                        res.finish = (status, description, data, rawData) => {
                            util.finish(res, status, description, data, rawData);
                        };
                        cb(req, res);
                    });
                });
                inst.use('/' + name, sub);
                next();
            })
            .catch((e) => {
                res.type('text/plain')
                    .status(500)
                    .send(e)
                    .end();
            });
        });
    return inst;
};

module.exports = (util) => api(util);