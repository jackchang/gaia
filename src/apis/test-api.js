module.exports = {
    get: (req, res) => {
        res.type('text/plain')
            .status(200)
            .send('url=[' + req.url + ']')
            .end();
    }
};