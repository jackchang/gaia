const url = require('url');
const path = require('path');
const riak = require(path.resolve('lib', 'riak.js'));

module.exports = {
    get: (req, res) => {
        const uri = url.parse(req.url, true);
        const kvs = uri.query;
        const key = Object.keys(kvs)[0];
        if (!key) {
            return res.finish(500, 'invalid query pattern');
        }

        riak.list('dashcam-photo', key, kvs[key])
            .then((l) => {
                res.finish(200, '', l);
            })
            .catch((e) => res.finish(500, e));
    }
};