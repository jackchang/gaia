const fs = require('fs');
const path = require('path');
const url = require('url');
const jsons = require('JSONStream');
const readline = require('readline');

const https = require('https');
const http = require('http');
const express = require('express');
const ws = require('ws').Server;
const riak = require(path.resolve('lib', 'riak.js'));

const logger = require(path.resolve('lib', 'logger.js'));
const timer = require(path.resolve('lib', 'timer.js'));
const config = require(path.resolve('config.js'));

module.exports = (util) => express.Router()
    .get('*', (req, res) => {
        const uri = require('url').parse(req.baseUrl);
        const tks = uri.pathname.split('/').filter((v) => !!v);
        const bkt = tks[0].substring(1);

        if (tks.length === 1) {
            const kvs = require('url').parse(req.url, true).query || {};
            const key = Object.keys(kvs)[0];
            const val = ((kvs[key] instanceof Array) ? kvs[key] : [kvs[key]]).map((v) => isNaN(v) ? v : v * 1.0);
            return riak.list(bkt, key, (val.length === 1) ? val[0] : val)
                .then((l) => util.finish(res, 200, '', l))
                .catch((e) => util.finish(res, 500, e));
        }

        if (tks.length === 2) {
            const key = tks[1];
            const type = req.headers['content-type'] || 'application/octet-stream';
            return riak.get(bkt, key)
                .then((d) => util.finish(res, 200, '', d, type))
                .catch((e) => util.finish(res, 404, e, ''));
        }

        util.finish(res, 404, 'invalid path components (1)');            
    })
    .delete('*', (req, res) => {
        const uri = require('url').parse(req.baseUrl);
        const tks = uri.pathname.split('/').filter((v) => !!v);
        if (tks.length !== 2) {
            util.finish(res, 404, 'invalid path components (1)');            
        }

        const bkt = tks[0].substring(1);
        const key = tks[1];
        return riak.delete(bkt, key)
            .then((d) => util.finish(res, 200))
            .catch((e) => util.finish(res, 404, e));
    })
    .put('*', (req, res) => {
        const uri = require('url').parse(req.baseUrl);
        const tks = uri.pathname.split('/').filter((v) => !!v);
        if (tks.length !== 2) {
            return util.finish(res, 404, 'invalid path components (2)');
        }

        const bkt = tks[0].substring(1);
        const key = tks[1];
        const idx = require('url').parse(req.url, true).query || {};
        Object.keys(idx).forEach((k) => {
            const v = idx[k];
            if (!isNaN(v)) {
                idx[k] = v * 1.0;
            }
        });
<<<<<<< .merge_file_pbMMw7
        idx.gaia_ts = new Date().getTime();
=======
        idx.gaia_ts = Date.now();
>>>>>>> .merge_file_OskqUl
        util.all(req, true).then((d) => {
            riak.index(idx)
                .put(bkt, key, d)
                .then(() => util.finish(res, 200))
                .catch((e) => util.finish(res, 404, e));
        }).catch((e) => util.finish(res, 404, e.toString()));
    });
