'use strict';

const fs = require('fs');
const path = require('path');
const url = require('url');
const jsons = require('JSONStream');
const readline = require('readline');

const https = require('https');
const http = require('http');
const express = require('express');
const ws = require('ws').Server;
const riak = require(path.resolve('lib', 'riak.js'));

const logger = require(path.resolve('lib', 'logger.js'));
const timer = require(path.resolve('lib', 'timer.js'));
const async = require(path.resolve('lib', 'async.js'));
const util = require(path.resolve('lib', 'http-util.js'));
const cluster = require(path.resolve('lib', 'cluster.js'));
const config = require(path.resolve('config.js'));

// https    
const privateKey  = fs.readFileSync(config.https.privateKey, 'utf8');
const certificate = fs.readFileSync(config.https.certificate, 'utf8');
const credentials = { key: privateKey, cert: certificate };

if (config.exceptions.catchAll) {
    require(path.resolve('lib', 'exception.js'));
}

cluster.launch(() => {    
    require(path.resolve('module-conf.js')).then(() => {
        // http engine
        const exp = express()
            .use('/apis', require(path.resolve('module-api.js'))(util))
            .use('/=*', require(path.resolve('module-bin.js'))(util))
            .use('/.*', require(path.resolve('module-cache.js'))(util))
            .use('/!*', require(path.resolve('module-cache.js'))(util))
            .use('/*', require(path.resolve('module-obj.js'))(util));

        const server = http.createServer(exp);
        const secure = https.createServer(credentials, exp);
        secure.listen(config.https.port, () => {
            logger.section('start worker ' + cluster.pid() + ' https [' + config.https.port + ']');
        });
        server.listen(config.port, () => {
            logger.section('start worker ' + cluster.pid() + ' http  [' + config.port + ']');
        });
    });
});