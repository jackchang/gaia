const path = require('path');
const cluster = require('cluster');
const config = require(path.resolve('config.js'));
const logger = require(path.resolve('lib', 'logger.js'));
const cpus = config.cpus || require('os').cpus().length;

const workers = [];
const handlers = {};

if (cluster.isMaster) {
    cluster.on('message', (worker, msg) => {
        module.exports.broadcast(msg);
    });
}
else {
    process.on('message', (raw) => {
        try {
            let msg = JSON.parse(raw);
            let key = msg.sender + '::' + msg.type;
            let l = handlers[key];
            (l) && (l.forEach((cb) => {
                try { cb(msg); } catch (exp) { logger.exception(exp); }
            }));
        }
        catch (e) {
            logger.exception(e);
        }
    });    
}

module.exports = {
    broadcast: (msg) => {
        if (typeof(msg) !== 'string') {
            msg = JSON.stringify(msg);
        }
        if (cluster.isMaster) {
            workers.forEach((worker) => {
                try { worker.send(msg); }
                catch (e) { console.error(e); }
            });
        }
        else {
            process.send(msg);
        }
    },
    on: (sender, type, cb) => {
        let key = sender + '::' + type;
        ((handlers[key]) || (handlers[key] = [])).push(cb);
    },
    pid: () => {
        return cluster.worker.id;
    },
    launch: (worker) => {
        if (!cluster.isMaster) {
            return worker();
        }

        logger.debug('launching ' + cpus + ' workers!');
        for (let i = 0; i < cpus; ++i) {
            let p = i + 1;
            let launch = (code, signal) => {
                if (code !== undefined) {
                    logger.debug('worker ' + p + ' exit with [' + code + '] [' + signal + ']');
                }
                workers[i] = cluster.fork().on('exit', launch);
            }
            launch();
        }
    }
};