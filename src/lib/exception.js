const fs = require('fs');
const path = require('path');
const logger = require(path.resolve('lib', 'logger.js'));

process.on('uncaughtException', (err) => {
    let date = Date.now();
    fs.writeFile('exception_' + date + '.txt', err.toString(), (exp) => {
    });
    logger.error(1);
    logger.error(err.stack);
    logger.error(err);
    logger.error(2);
});