const path = require('path');
const logger = require(path.resolve('lib', 'logger.js'));
const config = require(path.resolve('config.js'));

const defaults = {
    protocol: config.kvdb.protocol,
    host: '',//config.kvdb.host + ':' + config.kvdb.port,
    timeout: config.kvdb.timeout,
    type: 'application/octet-stream',
    restful: require(path.resolve('lib', 'restful'))[config.kvdb.protocol]
};

const runtime = {
    protocol: defaults.protocol,
    host: defaults.host,
    timeout: defaults.timeout,
    type: defaults.type,
    restful: defaults.restful,
    index: {}
};

const protocol = () => {
    const v = runtime.protocol;
    runtime.protocol = defaults.protocol;
    return v;
};
const host = () => {
    const v = runtime.host;
    runtime.host = defaults.host;
    return v;
};
const restful = () => {
    const v = runtime.restful;
    runtime.restful = defaults.restful;
    return v;
};
const timeout = () => {
    const v = runtime.timeout;
    runtime.timeout = defaults.timeout;
    return v;
};
const type = () => {
    const v = runtime.type;
    runtime.type = defaults.type;
    return v;
};
const index = () => {
    const v = runtime.index;
    runtime.index = {};
    return v;
};

const inst = (function() {
    let onload = [];
    if (!require('cluster').isMaster) {
        config.kvdb.host.forEach((h) => {
            const uri = 'http://' + h + ':' + config.kvdb.port + '/stats';
            const _restful = restful();
            _restful.get(uri)
                .then(() => defaults.host = runtime.host = h + ':' + config.kvdb.port)
                .then(() => onload.forEach((cb) => { try { cb(); } catch (e) { logger.warning(e); } }))
                .then(() => onload = null)
                .catch((e) => logger.warning);
        });
    }
    this.onload = (cb) => {
        if (onload) {
            onload.push(cb);            
        }
        else {
            process.nextTick(cb);
        }
    };
    this.protocol = (p) => {
        runtime.restful = require(path.resolve('lib', 'restful'))[runtime.protocol = p];
        return inst;
    };
    this.timeout = (ms) => {
        runtime.timeout = ms;
        return inst;
    },
    this.host = (name) => {
        runtime.host = name;
        return inst;
    };
    this.type = (_type) => {
        runtime.type = _type;
        return inst;
    };
    this.index = (_index) => {
        runtime.index = _index;
        return inst;
    };
    this.list = (bucket, index, value, maxCount) => {
        const _protocol = protocol();
        const _restful = restful();
        const _host = host();
        const _timeout = timeout();

        if (!bucket) {
            return _restful.get(_protocol + '://' + _host + '/buckets?buckets=true', _timeout)
                .then((r) => JSON.parse(r).buckets);
        }
        if (!index || !value) {
            return _restful.get(_protocol + '://' + _host + '/buckets/' + bucket + '/keys?keys=true', _timeout)
                .then((r) => JSON.parse(r).keys);
        }

        const _options = (maxCount) ? ('?max_results=' + maxCount) : '';
        const vtype = (value instanceof Array) ? typeof(value[0]) : typeof(value);
        const itype = (vtype === 'number') ? '_int/' : '_bin/';
        if (value instanceof Array) {
            if (value.length === 0) {
                return Promise.resolve([]);
            }

            if (value.length === 2 && vtype === 'number') {
                const max = (value[0] > value[1]) ? value[0] : value[1];
                const min = (value[0] > value[1]) ? value[1] : value[0];
                const _url = _protocol + '://' + _host + '/buckets/' + bucket + '/index/' + index + itype + min + '/' + max + _options;
                return _restful.get(_url, _timeout)
                    .then((r) => Object.keys(JSON.parse(r).keys.reduce((p, c) => { p[c] = true; return p; }, {})));
            }
            else {
                let vidx = 0;
                const results = {};
                const fetch = () => {
                    if (vidx === value.length) {
                        return Promise.resolve(Object.keys(results));
                    }
                    const val = value[vidx++];
                    const _url = _protocol + '://' + _host + '/buckets/' + bucket + '/index/' + index + itype + val;
                    return _restful.get(_url, _timeout).then((r) => {
                        const keys = JSON.parse(r).keys;
                        for (var i = 0; i < keys.length; ++i) {
                            const k = keys[i];
                            if (!results[k]) { results[k] = true; }
                        }
                    }).then(fetch);
                };
                return fetch();
            }
        }
        else {
            const _url = _protocol + '://' + _host + '/buckets/' + bucket + '/index/' + index + itype + value + _options;
            return _restful.get(_url, _timeout).then((r) => JSON.parse(r).keys);            
        }
    };
    this.stats = () => {
        const _protocol = protocol();
        const _restful = restful();
        const _host = host();
        const _timeout = timeout();

        return _restful.get(_protocol + '://' + _host + '/stats', _timeout)
            .then((r) => JSON.parse(r));
    };
    this.members = () => {
        return inst.stats()
            .then((r) => r.ring_members)
            .then((a) => a.map((n) => {
                var idx = n.indexOf('@');
                return (idx === -1) ? null : n.substring(idx + 1);
            }).filter((n) => !!n));
    };
    this.setBucketAttribute = (bucket, attributes) => {
        const _protocol = protocol();
        const _restful = restful();
        const _host = host();
        const _timeout = timeout();
        const _url = _protocol + '://' + _host + '/buckets/' + bucket + '/props';

        const headers = { 'Content-Type': 'application/json' };
        return _restful.put(_url, headers, { props: attributes }, _timeout);
    },
    this.enableMultiWrite = (bucket) => {
        return this.setBucketAttribute(bucket, { allow_mult: true });
    },
    this.disableMultiWrite = (bucket) => {
        return this.setBucketAttribute(bucket, { allow_mult: false });
    },
    this.setObjectMetaData = (bucket, key, attributes) => {
        const _protocol = protocol();
        const _restful = restful();
        const _host = host();
        const _timeout = timeout();
        const _url = _protocol + '://' + _host + '/buckets/' + bucket + '/keys/' + key;

        const headers = { 'Content-Type': 'application/octet-stream' };
        Object.keys(attributes).forEach((k) => {
            headers['x-riak-meta-' + k] = attributes[k] + '';
        });
        return this.setBucketAttribute(bucket, { allow_mult: true })
                   .then(() => _restful.put(_url, headers, '', _timeout));
    };
    this.put = (bucket, key, data, headers) => {
        const _protocol = protocol();
        const _restful = restful();
        const _host = host();
        const _type = type();
        const _index = index();
        const _timeout = timeout();
        const _url = _protocol + '://' + _host + '/buckets/' + bucket + '/keys/' + key;
        
        if (!headers) { headers = {}; }
        headers['Content-Type'] = _type || 'application/octet-stream';
        if (_index) {
            if (_index instanceof Array) {
                _index.forEach((kv) => {
                    var key = kv[0];
                    var val = kv[1];
                    var t = (val instanceof Array) ? typeof(val[0]) : typeof(val);
                    if (t === 'number') {
                        headers['x-riak-index-' + key + '_int'] = val;
                    }
                    else {
                        headers['x-riak-index-' + key + '_bin'] = val;
                    }
                    // headers['x-riak-meta-' + key] = val;
                });
            }
            else {
                Object.keys(_index).forEach((key) => {
                    var val = _index[key];
                    var t = (val instanceof Array) ? typeof(val[0]) : typeof(val);
                    if (t === 'number') {
                        headers['x-riak-index-' + key + '_int'] = val;
                    }
                    else {
                        headers['x-riak-index-' + key + '_bin'] = val;
                    }
                    // headers['x-riak-meta-' + key] = val;
                });
            }
        }
        return _restful.put(_url, headers, data, _timeout);
    };
    this.get = (bucket, key, parser, streamHandler) => {
        const _protocol = protocol();
        const _timeout = timeout();
        const _host = host();
        const _restful = restful();
        const _type = type();

        const uri = require('url').parse(_protocol + '://' + _host + '/buckets/' + bucket + '/keys/' + key);
        uri.method = 'GET';
        uri.headers = { 
            'Accept': (_type || 'application/octet-stream') + ',multipart/mixed'
        };

        return _restful.get(uri, _timeout, streamHandler)
            .then((r) => (parser) ? parser(r) : r);
    };
    this.delete = (bucket, key) => {
        const _protocol = protocol();
        const _restful = restful();
        const _host = host();
        const _timeout = timeout();

        return _restful.delete(_protocol + '://' + _host + '/buckets/' + bucket + '/keys/' + key, _timeout)
            .then((code) => {
                if (code !== 204 && code != 404) {
                    return Promise.reject('status code: ' + code);
                }
            });

    };
    return this;
}).call({});

module.exports = inst;


