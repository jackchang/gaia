const fs = require('fs');
const path = require('path');
const url = require('url');
const jsons = require('JSONStream');
const readline = require('readline');
const vm = require('vm');
const serdes = require('node-serialize');
const sugar = require('sugar');
const https = require('https');
const http = require('http');
const express = require('express');
const ws = require('ws').Server;

const riak = require(path.resolve('lib', 'riak.js'));
const restful = require(path.resolve('lib', 'restful.js'));
const util = require(path.resolve('lib', 'http-util.js'));
const logger = require(path.resolve('lib', 'logger.js'));
const timer = require(path.resolve('lib', 'timer.js'));
const schema = require(path.resolve('lib', 'schema-core.js'))

const config = require(path.resolve('config.js'));

const valueArray = (vals) => {
    return ((vals instanceof Array) ? vals : [vals]).map((v) => isNaN(v) ? v : v * 1.0);
};

const find = (bkt, key, vals) => {
    let val = valueArray(vals);
    return riak.list(bkt, key.replace(/\./g, '_'), (val.length === 1) ? val[0] : val);
};

const types = {};
const newSchema = (bkt, code) => {
    const validators = {};
    const onReject = [];
    const doValidation = (objs) => {
        let keys = Object.keys(validators);
        if (keys.length === 0) {
            return objs;
        }

        let fncs = keys.map((k) => validators[k]);
        let ok = [];
        let ng = [];
        let check = (obj) => {
            for (let i = 0; i < fncs.length; ++i) {
                let fn = fncs[i];
                if (!fn(obj)) { return ng.push([obj, keys[i]]); }
            }
            ok.push(obj);
        };

        try {
            for (let i = 0; i < objs.length; ++i) {
                check(objs[i]);
            }
        }
        catch (e) { logger.error(e); }

        if (ng.length !== 0) {
            onReject.forEach((fn) => {
                try { fn(ng); }
                catch (e) { logger.error(e); }
            });
        }
        return ok;
    };

    const onPut= [];
    const onPutCallback = (objs) => {
        if (onPut.length === 0) {
            return Promise.resolve(objs);
        }

        try { 
            let pl = onPut.map((cb) => cb(objs)).filter((p) => !!p);
            if (pl.length === 0) {
                return Promise.resolve(objs);
            }
            return Promise.all(pl).then(() => {
                return objs;
            });
        }
        catch (e) { 
            logger.warning(e); 
            return Promise.reject(e);
        }
    };

    const enableMultiWrite = (d) => {
        if (!(d instanceof Array)) {
            return Promise.resolve([d]);
        }

        if ((def.multiValue) || (def.idgen) || (def.shadowClass)) { return Promise.resolve(d); }

        return riak.enableMultiWrite(bkt).then(() => {
            def.multiValue = true;
            return d;
        });
    };

    let queue = [];
    const indexer = {};
    const pushObjects = (oid, oidx, olist, callback) => {
        queue.push([oid, oidx, olist, callback]);
        if (queue.length !== 1) {
            return;
        }

        setTimeout(() => {
            const groups = {};

            const pending = queue.map((g) => g[3]);
            const fin = (s, d) => {
                pending.forEach((cb) => { cb(s, d); });
            };

            let idgen = def.idgen;
            queue.forEach((g) => {
                let id = g[0];
                let idx = g[1];
                let objs = g[2];
                if (id) {
                    ((groups[id]) || (groups[id] = [])).push([idx, objs]);                    
                }
                else {
                    objs.forEach((obj, _i) => {
                        id = idgen(obj);
                        ((groups[id]) || (groups[id] = [])).push([idx, objs]);                    
                    });
                }
            });
            queue = [];

            let i = 0;
            const gkeys = Object.keys(groups);
            const indexf = Object.keys(indexer).map((k) => indexer[k]);
            const put = () => {
                if (i === gkeys.length) {
                    return fin(200, '');
                }

                let id = gkeys[i++];
                let ol = groups[id];
                let oa = [];
                // console.info('put ' + ol.length + ' ' + group);
                let idx = ol.reduce((a, kv) => { 
                    let kvs = kv[0];
                    (kvs) && (Object.keys(kvs).forEach((k) => {
                        let v = kvs[k];
                        ((a[k]) || (a[k] = {}))[v] = 1;
                    }));

                    let objs = kv[1];
                    (indexf.length) && (indexf.forEach((fn) => {
                        objs.forEach((obj) => {
                            fn(obj).forEach((v) => {
                                ((a[v[0]]) || (a[v[0]] = {}))[v[1]] = 1;
                            });
                        });
                    }));
                    oa = oa.concat(objs);
                    return a;
                }, {
                    gaia_ts: [Date.now()]
                });
                Object.keys(idx).forEach((k) => idx[k] = Object.keys(idx[k]).map((v) => isNaN(v) ? v : v * 1.0));

                riak.index(idx)
                    .put(bkt, id, oa)
                    .then(put)
                    .catch((e) => {
                        logger.error(e);
                        fin(404, e);
                    });
            };
            put();
        }, 500);
    };

    const putObjects = (id, idx, d) => {
        return enableMultiWrite(d)
        .then((objs) => doValidation(objs))
        .then((objs) => def.onPutCallback(objs))
        .then((objs) => {
            if (def.shadowClass) {
                return Promise.resolve([200, '']);
            }

            return new Promise((ok) => {
                pushObjects(id, idx, objs, (s, d) => ok([s, d]));
            });
        });
    };

    const getObject = (bkt, oid, util) => {
        let def = types[bkt];
        return riak.get(bkt, oid, null, (resp, ok, ng) => {                
            if (resp.statusCode === 404 || resp.statusCode === 204) {
                return ng(resp.statusMessage);
            }

            if (resp.statusCode !== 300) {
                return util.all(resp).then((d) => {
                    let o = JSON.parse(d);
                    if (o instanceof Array) { ok(o); }
                    else { ok([o]); }
                }).catch(ng);
            }

            let ct = resp.headers['content-type'].split(';')[1];
            if (!ct) { return ng('boundary missing'); }

            let kv = ct.split('=');
            if ((kv[0] || '').trim().toLowerCase() !== 'boundary') { return ng('invalid boundary key'); }
            if (!kv[1]) { return ng('empty boundary string'); }

            let ps = '--' + kv[1];
            let parts = [];
            let body = null;
            let rl = require('readline').createInterface({ input: resp });
            rl.on('line', (ln) => {
                if (!ln) { return body = []; }
                if (!body) { return; }
                if (!ln.startsWith(ps)) { return body.push(ln); }

                if (body.length) {
                    parts.push(body.join(''));
                }
                body = null;
            });
            resp.on('end', () => {
                if (!parts.length) { return ok([]); }

                let objs = parts.map(JSON.parse).reduce((p, o) => {
                    return ((o instanceof Array) ? (p = p.concat(o)) : p.push(o)) && (p);
                }, []);

                if (objs.length === 0) {
                    return ok([]);
                }
                
                let substitute = 0;
                if (def && def.merger) {
                    try {
                        let merged = def.merger(objs);
                        if (merged !== undefined) {
                            substitute = objs.length;
                            objs = merged;
                        }
                    }
                    catch (e) {
                        logger.warning(e);
                        ng(e);
                    }
                }

                if (!objs || objs.length === 0) { return ok([]); }

                ((substitute) ? def.onPutCallback(objs) : Promise.resolve(objs)).then(() => {
                    const indexer = (def) ? def.indexer : {};
                    const indexf = Object.keys(indexer).map((k) => indexer[k]);
                    const idx = (indexf.length) ? objs.reduce((a, obj) => { 
                        indexf.forEach((fn) => {
                            fn(obj).forEach((v) => {
                                ((a[v[0]]) || (a[v[0]] = {}))[v[1]] = 1;
                            });
                        });
                        return a;
                    }, {}) : {};
                    Object.keys(idx).forEach((k) => idx[k] = Object.keys(idx[k]));
                    // console.info(idx);
                    
                    idx.gaia_ts = Date.now();
                    riak.index(idx)
                        .put(bkt, oid, objs, { 'x-riak-vclock': resp.headers['x-riak-vclock'] })
                        .then(() => ok(objs)).catch(ng);
                });
            });
        })
    };

    const getObjects = (info, forceKey) => {
        if (typeof(info) === 'string') {
            return getObject(bkt, info, util);
        }

        let kvs = info;
        let kcount = Object.keys(kvs).length
        if (kcount === 0) {
            return Promise.reject('invalid query');
        }

        let key = (forceKey) || (((kcount !== 1) ? (def.searchOrder.reduce((p, c) => {
            return p || ((kvs[c] !== undefined) ? c : '');
        }, '')) : '') || Object.keys(kvs)[0]);
        let kvlist = Object.keys(kvs).map((k) => [k, valueArray(kvs[k])]);
        let filters = kvlist.map((kvi) => {
            if (kvi[0] === key && ((!def) || (!def.indexer[key]))) {
                return null;
            }

            let kl = kvi[0].split('.');
            let vl = kvi[1];
            if (vl.length === 1) {
                let v0 = vl[0];
                return (obj) => {
                    let i = 0;
                    let v = obj[kl[i++]];
                    while ((v) && (i !== kl.length)) {
                        v = v[kl[i++]];
                    }
                    return (v instanceof Array) ? v.indexOf(v0) !== -1 : v === v0;
                };
            }
            else {
                let max = (vl[0] > vl[1]) ? vl[0] : vl[1];
                let min = (vl[0] > vl[1]) ? vl[1] : vl[0];
                return (obj) => {
                    let i = 0;
                    let v = obj[kl[i++]];
                    while ((v) && (i !== kl.length)) {
                        v = v[kl[i++]];
                    }
                    return (v instanceof Array) ? 
                        (v.filter(vi => vi >= min && vi <= max).length !== 0) : 
                        (v >= min && v <= max);
                };
            }
        }).filter((f) => !!f);
        let filter = (obj) => {
            try { return filters.reduce((p, cb) => p && cb(obj), true); }
            catch (e) { logger.error(e); return false; }
        };
        return find(bkt, key, kvs[key])
            .then((l) => Object.keys(l.reduce((p, k) => ((p[k] = true) && p), {})))
            .then((l) => filterIds(info, l))
            .then((l) => new Promise((ok, ng) => {
                var i = 0;
                let objs = [];
                let fetch = () => {
                    if (i === l.length) {
                        try { ok(objs.filter(filter)); }
                        catch (e) { ng(e); }
                    }
                    getObject(bkt, l[i++], util)
                        .then((a) => objs = objs.concat(a))
                        .then(fetch)
                        .catch(ng);
                };
                fetch();
            }));
    };

    const idFilters = [];
    const filterIds = (conditions, ids) => {
        if (idFilters.length === 0) { return ids; }
        return ids.filter((id) => {
            return idFilters.reduce((p, fn) => {
                try { return (p) && (fn(conditions, id) !== false); }
                catch (e) {
                    logger.warning(e);
                    return p;
                }
            }, true);
        });
    };

    const onAgeout = [];
    const onAgeoutCallback = (key, obj) => {
        for (let i = 0; i < onAgeout.length; ++i) {
            try { onAgeout[i](key, obj); }
            catch (e) { logger.warning(e); }
        }
    };

    const def = types[bkt] = {
        loaded: true,

        multiValue: false,
        shadowClass: false,
        indexer: indexer,
        validators: validators,
        onPut: onPut,
        onReject: onReject,
        onAgeout: onAgeout,
        searchOrder: [],
        searchFilter: idFilters,

        idgen: null,
        merger: null,
        enableMultiWrite: enableMultiWrite,
        onPutCallback: onPutCallback,
        onAgeoutCallback: onAgeoutCallback,
        putObjects: putObjects,
        getObjects: getObjects
    };
    return def;
};



const cache = {};
setInterval(() => {
    Object.keys(cache).forEach((bucketName) => {
        cache[bucketName][1]();
    });
}, 5000);

const handles = {};
const createHandle = (bucket) => {
    const cacheBucket = '.' + bucket;
    const cacheHandle = (cache[bucket]) || (cache[bucket] = [{}, () => {
        const cacheCallback = def().onAgeoutCallback;
        const now = Date.now();
        Object.keys(localCache)
            .filter((key) => { return (localCache[key][0] < now); })
            .forEach((key) => {
                let obj = localCache[key][1];
                process.nextTick(() => {
                    cacheCallback(key, obj);
                });
                delete localCache[key];
            });
    }]);
    const localCache = cacheHandle[0];

    let defi = null;
    const def = () => {
        if (defi) {
            return defi;
        }
        // console.info('get ' + bucket);
        return defi = ((types[bucket]) ? types[bucket] : newSchema(bucket, 1));
    };

    const inst = {
        put: (id, index, obj) => {
            if (obj === undefined) {
                if (index === undefined) {
                    obj = id;
                    id = null;                    
                }
                else {
                    obj = index;
                    index = null;
                }
            }

            return def().putObjects(id, index, obj);
        },
        get: (info, cacheTime) => {
            if (cacheTime === undefined) {
                return def().getObjects(info);                
            }

            if (cacheTime === 0) {
                cacheTime = config.cache.defaultTTL;
            }

            let key = (typeof(info) === 'string') ? info : JSON.stringify(info);
            let r = localCache[key];
            if (r) { 
                return Promise.resolve(r[1]); 
            }

            let t = Date.now() + (cacheTime * 1000);
            return def().getObjects(info)
                .then((a) => {
                    if (!a) { return Promise.reject('not exist'); }

                    localCache[key] = [t, a];
                    return a;
                });
        },

        cache: (key, value, ttl) => {
            if (ttl === undefined) {
                ttl = config.cache.defaultTTL;
            }

            let r = (localCache[key]) || (localCache[key] = [null, null]);
            r[0] = Date.now() + (ttl * 1000);
            r[1] = value;
            return riak
                .index({ 
                    gaia_ts: Date.now()
                }).put(cacheBucket, key, { t: r[0], d: r[1] });
        },
        recall: (key, refetchThreshold) => {
            if (refetchThreshold === undefined) {
                refetchThreshold = config.cache.defaultRefetchThreshold;
            }

            let now = Date.now();
            let r = localCache[key];
            let ts = now - (refetchThreshold * 1000);
            if ((r) && (r[0] >= ts)) {
                return Promise.resolve(r[1]);
            }

            return riak.get(cacheBucket, key).then((d) => {
                if (!d) {
                    return null;
                }
                localCache[key] = [now, d.d];
                return d.d;
            }).catch((e) => Promise.resolve(null));
        },
        onAgeout: (cb) => {
            def().onAgeout.push(cb);
            return inst;
        },

        multiValue: (b) => {
            if (b && !def().multiValue) {
                riak.enableMultiWrite(bucket).then(() => {
                    def().multiValue = true;                    
                })
            }
            else if (!b && def().multiValue) {
                riak.disableMultiWrite(bucket).then(() => {
                    def().multiValue = false;
                });                                
            }
            return inst;
        },
        searchOrder: (keys) => {
            (keys) && (def().searchOrder = keys);
            return inst;
        },
        searchFilter: (cb) => {
            def().searchFilter.push(cb);
            return inst;
        },
        require: (p) => {
            if (typeof(p) === 'string') {
                p = [p];
            }
            p.forEach((n) => {
                inst.validate('property-' + n + '-required', (obj) => obj[n] !== undefined);
            });
            return inst;
        },
        notNull: (p) => {
            if (typeof(p) === 'string') {
                p = [p];
            }
            p.forEach((n) => {
                inst.validate('property-' + n + '-required', (obj) => {
                    let v = obj[n];
                    return v !== undefined && v !== null;
                });
            });
            return inst;
        },
        notEmpty: (p) => {
            if (typeof(p) === 'string') {
                p = [p];
            }
            p.forEach((n) => {
                inst.validate('property-' + n + '-required', (obj) => !!obj[n]);
            });
            return inst;
        },
        inRange: (p, l, u) => {
            if (isNaN(l)) {
                return inst.validate('property-' + p + '-less-than' + u, (obj) => {
                    let v = obj[p] * 1;
                    return p <= u;
                });
            }
            if (isNaN(u)) {
                return inst.validate('property-' + p + '-greater-than-' + l, (obj) => {
                    let v = obj[p] * 1;
                    return p >= l;
                });
            }
            return inst.validate('property-' + p + '-range-' + l + '-' + u, (obj) => {
                let v = obj[p] * 1;
                return p >= l && p <= u;
            });
        },
        validate: (name, fn) => {
            def().validators[name] = fn;
            return inst;                
        },
        watch: (ps) => {
            if (typeof(ps) === 'string') {
                ps = [ps];
            }
            ps.forEach((p) => {
                const keys = p.split('.');
                const pkey = keys.join('_');
                def().indexer[p] = (obj) => {
                    var i = 0;
                    var v = obj[keys[i++]];
                    while ((v) && (i !== keys.length)) {
                        v = v[keys[i++]];
                    }
                    if (v === undefined) { return []; }

                    if (v instanceof(Array)) {
                        return v.map((sv) => [pkey, sv]);
                    }
                    if (typeof(v) === 'object') {
                        return Object.keys(v).map((sk) => {
                            return [pkey + '_' + sk, v[sk]];
                        });
                    }
                    return [[pkey, v]];
                };
            });
            return inst;
        },
        onReject: (cb) => {
            def().onReject.push(cb);
            return inst;
        },
        onPut: (cb) => {
            def().onPut.push(cb);
            return inst;
        },
        merger: (cb) => {
            def().merger = cb;
            return inst;
        },
        shadowClass: (b) => {
            def().shadowClass = b;
            return inst;
        },
        autoBuild: () => {
            let ikeys = Object.keys(def().indexer);
            let rid = Date.now();
            if (ikeys.length === 0) {
                if (!def().idgen) {
                    throw 'auto table must contain at least one index';
                } 
            }
            else {
                let getters = ikeys.map((k) => def().indexer[k]);
                let getkvs = (obj) => getters.reduce((p, f) => {
                    f(obj).forEach((kv) => {
                        let k = kv[0];
                        let v = kv[1];
                        ((p[k]) || (p[k] = [])).push(v);
                    });
                    return p;
                }, {});
                def().idgen = (obj) => {
                    let kvs = getkvs(obj);
                    let tks = Object.keys(kvs).map((k) => {
                        let l = kvs[k].map((v) => new Buffer(v + '').toString('base64'));
                        return k + ',' + l.join(',');
                    });
                    return tks.join('-');
                };
                def().searchFilter.push((conditions, id) => {
                    let kvs = id.split('-').map((t) => t.split(','));
                    return kvs.reduce((p, tks, qi) => {
                        if (!p) { return p; }
                        if (tks.length === 1) { return p; }

                        let key = tks[0];
                        let vals = conditions[key];
                        if (!vals) { return p; }

                        if (typeof(vals) === 'string') {
                            for (let i = 1; i < tks.length; ++i) {
                                let v = Buffer.from(tks[i], 'base64').toString();
                                if (v === vals) {
                                    return true;
                                }
                            }
                            return false;
                        }
                        else {
                            let vmax = (vals[0] > vals[1]) ? vals[0] : vals[1];
                            let vmin = (vals[0] > vals[1]) ? vals[1] : vals[0];
                            if (!isNaN(vmax)) {
                                vmax *= 1; vmin *= 1;
                            }
                            for (let i = 1; i < tks.length; ++i) {
                                let v = Buffer.from(tks[i], 'base64').toString();
                                if (v >= vmin && v <= vmax) {
                                    return true;
                                }
                            }
                            return false;

                        }
                    }, true);
                });
                return inst;
            }
            return inst;
        },
        id: (fn, delimiter) => {
            if (typeof(fn) === 'string') {
                inst.notEmpty(fn);
                def().idgen = (obj) => obj[fn] || '';
            }
            else if (fn instanceof Array) {
                fn.forEach((n) => inst.notEmpty(n));
                if (!delimiter) { delimiter = '-'; }
                def().idgen = (obj) => fn.map((n) => obj[n] || '').join(delimiter);
            }
            else {
                def().idgen = (obj) => {
                    try { return fn(obj) }
                    catch (e) { logger.error(e); return null; }
                };                
            }
            return inst;
        }
    };
    return inst;
};

const getHandle = (bucket) => {
    return (handles[bucket]) || (handles[bucket] = createHandle(bucket));
};
getHandle.now = (format) => {
    let n = new Date();
    if (format === undefined) { return n; }
    if (typeof(format) === 'number') { return n.getTime() + format; }
    return sugar.Date.format(n, format);
};

const reloadSchema = () => {
    return riak.list('::sys', 'type', 'schema')
        .then((l) => {
            let all = l.map((f) => {
                return riak.type('*').get('::sys', f).then((scr) => {
                    try {
                        let s = [scr, 'try { module.exports(x) } catch (e) { error = true; exp = e; }'].join(';');
                        let sandbox = { 
                            console: console,
                            logger: logger,
                            require: require, 
                            module: {}, 
                            x: getHandle,
                            error: false,
                            exp: null
                        };
                        let script = new vm.Script(s);
                        let context = new vm.createContext(sandbox);
                        script.runInContext(context);                        
                        if (sandbox.error) {
                            return Promise.reject({
                                record: f,
                                message: sandbox.exp
                            });
                        }                        
                    }
                    catch (e) {
                        return Promise.reject({
                            record: f,
                            message: require('syntax-error')(scr)
                        });
                    }
                });
            });
            Object.keys(types).forEach((k) => delete types[k]);
            Object.keys(handles).forEach((k) => delete handles[k]);
            return Promise.all(all);
        });
};


module.exports = {
    types: types,
    new: newSchema,
    find: find,
    reload: reloadSchema
};