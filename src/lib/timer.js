var timer = (() => {
    var idx = 1;

    const s1 = [];
    const s10 = [];
    const call = (list) => {
        list.filter((cb) => { return cb() === false; })
            .forEach((cb) => { 
                var k = list.indexOf(cb);
                if (k !== -1) {
                    list.splice(k, 1);
                }
            });
    };

    setInterval(() => {
        if ((idx % 10) === 0) { 
            call(s10);
        }
        call(s1);
        idx++;
    }, 1000);

    const add = (l, cb) => { l.push(cb); };
    const rem = (l, cb) => {
        var k = l.indexOf(cb);
        if (k !== -1) {
            l.splice(k, 1);
        }
    };
    return { 
        s1:  (cb, remove) => (remove) ? rem(s1, cb) :  add(s1, cb),
        s10: (cb, remove) => (remove) ? rem(s10, cb) : add(s10, cb)
    };
})();

module.exports = timer;