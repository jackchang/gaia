module.exports = (f) => {
    console.info('check ' + f);
    var src = require('fs').readFileSync(f);
    var err = (require('syntax-error'))(src.toString(), f);
    if (err) {
        console.error('ERROR DETECTED' + Array(62).join('!'));
        console.error(err);
        console.error(Array(76).join('-'));
    }
};