const path = require('path');
const readline = require('readline');
const logger = require(path.resolve('lib', 'logger.js'));

module.exports = {
    all: (resp) => {
        const data = [];
        return new Promise((ok, ng) => {
            resp.on('data', (chunk) => { data.push(chunk); })
                .on('error', ng)
                .on('end', () => { ok(Buffer.concat(data)); });        
        });
    },
    allLines: (resp) => {
        return new Promise((ok, ng) => {
            const lines = [];
            readline.createInterface({ input: resp })
                .on('error', ng)
                .on('line', (l) => lines.push(l))
                .on('close', () => { ok(lines); });
        });
    },
    finish: function (resp, status, description, data, rawData) {
        if (resp.headersSent) {
            try { resp.end(); } catch (e) { logger.info(e); }            
        }
        else {
            try {
                if (rawData) {
                    resp.type(rawData)
                        .set({
                            'Access-Control-Allow-Origin': '*',
                            'Access-Control-Allow-Methods': '*',
                        })
                        .status(200)
                        .send(data)
                        .end();
                }
                else {
                    resp.type('json')
                        .set({
                            'Access-Control-Allow-Origin': '*',
                            'Access-Control-Allow-Methods': '*',
                        })
                        .status(200)
                        .json({ status: status, description: description, data: data || {} })
                        .end();                
                }
            }
            catch (e) { logger.error(e); }
        }
    }
};