module.exports = {
    forEach: (items, callback) => {
        let keys = (items instanceof Array) ? items : Object.keys(items);
        let vals = (keys === items) ? items : keys.map((k) => items[k]);
        return new Promise((resolve, reject) => {
            let i = 0;
            const cb = (err) => {
                if (err) { return reject(err); }
                work();
            };
            const work = () => {
                if (i === keys.length) { return resolve(); }

                try { callback(keys[i], vals[i], i++, cb); }
                catch (e) { reject(e); }
            };
            work();
        });
    },
    map: (items, callback) => {
        let values = new Array(items.length);
        return new Promise((resolve, reject) => {
            let i = 0;
            const cb = (err, data) => {
                if (err) { return reject(err); }
                values[i++] = data;
                work();
            };
            const work = () => {
                if (i === items.length) { return resolve(values); }

                try { callback(items[i], i, cb); }
                catch (e) { reject(e); }
            };
            work();
        });
    },
    reduce: (items, callback, initValue) => {
        return new Promise((resolve, reject) => {
            let i = 0;
            const cb = (err, data) => {
                if (err) { return reject(err); }
                initValue = data;
                work();
            };
            const work = () => {
                if (i === items.length) { return resolve(initValue); }

                try { callback(initValue, items[i], i++, cb); }
                catch (e) { reject(e); }
            };
            work();
        });
    },
    filter: (items, callback) => {
        let values = new Array();
        return new Promise((resolve, reject) => {
            let i = 0;
            const cb = (err, accept) => {
                if (err) { return reject(err); }
                if (accept) { values.push(items[i]); }
                work(i++);
            };
            const work = () => {
                if (i === items.length) { return resolve(values); }

                try { callback(items[i], i, cb); }
                catch (e) { reject(e); }
            };
            work();
        });
    }
};
