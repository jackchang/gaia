const path = require('path');
const logger = require(path.resolve('lib', 'logger.js'));
const busboy = require('busboy');

Function.prototype.promisify = function() {
    return new Promise(this);
};

var all = function(resp, ok, ng) {
    if (resp.statusCode >= 400) {
        return ng('(' + resp.statusCode + ')' + resp.statusMessage);
    }

    var data = [];
    var length = 0;
    resp.on('data', (chunk) => { length += chunk.length; data.push(chunk); })
        .on('error', ng)
        .on('end', () => { ok(Buffer.concat(data, length)); });
};

var func = (http) => { return {
    readAll: (resp, ok, ng) => all(resp, ok, ng),
    get: (url, timeout, streamHandler) => {
        return ((resolve, reject) => {
            if (typeof(url) === 'string') {
                url = require('url').parse(url);
                url.method = 'GET';
            }

            var cb = streamHandler || all;
            var u = require('url').format(url);
            // logger.info('restful -     get ' + u);
            var req = http.request(url)
                .on('response', (rs) => { 
                    try {
                        cb(rs, (d) => { 
                            // logger.info('restful - end get ' + u + ' ' + rs.statusCode);
                            resolve(d); 
                        }, reject, all); 
                    }
                    catch (err) { reject(err); }
                })
                .on('error', (e) => { 
                    // logger.info('restful -     get ' + u);
                    // logger.error(e); 
                    reject(e); 
                })
                .on('socket', (socket) => {
                    socket.setTimeout(timeout || (3 * 1000));
                    socket.on('timeout', () => { req.abort(); });
                });
            req.end('');
        }).promisify();
    },
    put: (url, headers, data, timeout) => {
        return ((resolve, reject) => {
            var uri = require('url').parse(url);
            uri.method = 'PUT';
            uri.headers = headers;
            var req = http.request(uri)
                .on('response', (rs) => { all(rs, () => {
                    if (rs.statusCode >= 400) {
                    logger.error('restful - end put ' + url + ' ' + rs.statusCode);                        
                    }
                    // logger.info('restful - end put ' + url + ' ' + rs.statusCode);
                    resolve();
                }, reject); })
                .on('error', reject)
                .on('socket', (socket) => {
                    socket.setTimeout(timeout || (3 * 1000));
                    socket.on('timeout', () => { 
                        req.abort(); 
                    });
                    socket.on('close', (err) => { 
                        if (err) {
                            // logger.info('restful - err - w(' + socket.bytesWritten + ') r(' + socket.bytesRead + ') - ' + err); 
                        }
                    });
                });

            var content = (typeof(data) === 'object' && !(data instanceof Buffer)) ? JSON.stringify(data) : data;
            req.write(content, () => { 
                // logger.info('end put');
                req.end(); 
            });
            // logger.info('restful -     put ' + url);
        }).promisify();
    },
    delete: (url, timeout) => {
        return ((resolve, reject) => {
            var uri = require('url').parse(url);
            uri.method = 'DELETE';
            var req = http.request(uri)
                .on('response', (rs) => { 
                    // logger.info('restful - end delete ' + url + ' ' + rs.statusCode); 
                    req.end();
                    resolve(rs.statusCode); 
                })
                .on('error', (err) => {
                    reject(err);
                })
                .on('socket', (socket) => {
                    req.end();
                    socket.setTimeout(timeout || (3 * 1000));
                    socket.on('timeout', () => { req.abort(); });
                });
            // logger.info('restful -     delete ' + url);
        }).promisify();            
    }
}; };

module.exports = {
    http: func(require('http')),
    https: func(require('https'))
};
