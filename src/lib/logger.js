const path = require('path');
const chalk = require('chalk');

module.exports = {
    section:    function()  { console.info(chalk.green.bold(' - ' + [].join.call(arguments, ''))); },
    write:      function()  { console.info(     chalk.white('   ' + [].join.call(arguments, ''))); },
    info:       function()  { console.info(     chalk.white('   ' + [].join.call(arguments, ''))); },
    error:      function()  { console.info(  chalk.red.bold('   ' + [].join.call(arguments, ''))); },
    warning:    function()  { console.info(    chalk.yellow('   ' + [].join.call(arguments, ''))); },
    debug:      function()  { console.info(      chalk.gray('   ' + [].join.call(arguments, ''))); },
    exception:  function(e) { console.info(  chalk.red.bold('   exp: ' + e)); }
};
