const fs = require('fs');
const path = require('path');
const url = require('url');
const jsons = require('JSONStream');
const readline = require('readline');
const https = require('https');
const http = require('http');
const express = require('express');
const ws = require('ws').Server;
const riak = require(path.resolve('lib', 'riak.js'));

const logger = require(path.resolve('lib', 'logger.js'));
const timer = require(path.resolve('lib', 'timer.js'));
const schema = require(path.resolve('lib', 'schema-core.js'))
const config = require(path.resolve('config.js'));

const getTTL = (uri) => {
    const s = (uri.search || '?').substring(1);
    if (isNaN(s)) {
        return ((uri.query || {}).ttl || 300 * 10000000) / 10000;
    }
    return ((s * 1) / 10000) || 300 * 1000;
};

const put = (util, req, res) => {
    const uri = url.parse(req.baseUrl, true);
    const tks = uri.pathname.split('/').filter((v) => !!v);
    const bkt = tks[0];
    const ttl = getTTL(url.parse(req.url, true));
    const now = Date.now();
    const exp = now + ttl;
    if (tks.length === 2) {
        util.all(req).then((l) => {
            l = l.toString();
            riak.index({ gaia_ts: now })
                .put(bkt, tks[1], { ts: now, exp: exp, data: l })
                .then(() => util.finish(res, 200))
                .catch((e) => util.finish(res, 404, e));
        });
    }
    else {
        util.allLines(req)
            .then((lines) => lines.map((l) => {
                const idx = l.indexOf(' ');
                if (idx === -1) {
                    return null;
                }
                return [l.substring(0, idx), l.substring(idx + 1)];
            }))
            .then((kvs) => kvs.filter((kv) => !!kv))
            .then((kvs) => {
                return Promise.all(kvs.map((kv) => {
                    return riak.index({ gaia_ts: now })
                        .put(bkt, kv[0], { ts: now, exp: exp, data: kv[1] });
                }))
            })
            .then(() => util.finish(res, 200))
            .catch((e) => util.finish(res, 404, e));
    }
};
const str2obj = (d) => { try { return JSON.parse(d); } catch (e) { return null; } };
const obj2str = (o) => {
    if (!o) { return ''; }
    if (typeof(o) === 'string') { return o; }
    return JSON.stringify(o);
};
const get = (util, req, res) => {
    const uri = url.parse(req.baseUrl, true);
    const tks = uri.pathname.replace('!', '.').split('/').filter((v) => !!v);
    const bkt = tks[0];
    const opt = url.parse(req.url, true).query || {};
    const now = (opt.all) ? -1 : Date.now();
    const pth = opt.key || '';
    const getter = (pth || (tks.length > 2)) ? (function() {
        let l = (pth) ? pth.split('.') : tks.splice(2);
        return (d) => {
            if (!d) { return ''; }
            let o = str2obj(d);
            for (let i = 0; i < l.length; ++i) {
                if (!o) { break; }
                let idx = l[i];
                if (o instanceof Array) {
                    let ii = idx.split(',').map((v) => {
                        let i0 = v * 1;
                        return (i0 < 0) ? i0 + o.length : i0;
                    });
                    if (ii.length === 1) { o = isNaN(ii[0]) ? o[idx] : o[ii[0]]; }
                    else if (ii[0] > ii[1]) { o = o.slice(ii[1], ii[0] + 1); }
                    else { o = o.slice(ii[0], ii[1] + 1); }
                }
                else {
                    o = o[idx];
                }
            }
            return (o) ? obj2str(o) : '';
        };
    })() : ((d) => d);
    if (tks.length >= 2) {
        const key = tks[1];
        riak.get(bkt, key, JSON.parse)
            .then((obj) => {
                if (obj.exp >= now) {
                    util.finish(res, 200, '', key + ' ' + getter(obj.data), 'text/plain');
                }
                else {
                    util.finish(res, 200, '', key + ' ', 'text/plain');
                }
            })
            .catch((e) => util.finish(res, 404, e));
    }
    else {
        const kvs = [];
        util.allLines(req)
            .then((keys) => {
                if (!opt.ts) { return Promise.resolve(keys); }
                if (typeof(opt.ts) === 'string') {
                    opt.ts = [opt.ts * 1, Date.now()];
                }
                else {
                    opt.ts = opt.ts.map((v) => v * 1);
                }

                return riak.list(bkt, 'gaia_ts', opt.ts)
                    .then((list) => list.filter((k) => keys.length === 0 || keys.indexOf(k) !== -1));
            })
            .then((keys) => {
                return Promise.all(keys.map((k) => riak.get(bkt, k, JSON.parse).then((obj) => {
                    kvs.push((obj.exp >= now || opt.ts) ? (k + ' ' + getter(obj.data)) : (k + ' '));
                })));
            })
            .then(() => util.finish(res, 200, '', kvs.join('\n'), 'text/plain'))
            .catch((e) => {
                console.info(e);
                util.finish(res, 404, e);
            });
    }
};

module.exports = (util) => express.Router()
    .get('*', (req, res) => get(util, req, res))
    .post('*', (req, res) => {
        (req.baseUrl[1] === '!') ? get(util, req, res) : put(util, req, res);
    })
    .put('*', (req, res) => {
        (req.baseUrl[1] === '!') ? get(util, req, res) : put(util, req, res);
    });
