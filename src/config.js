module.exports = {
    cpus: 0,    // auto, use all cores
    port: 8001, // HTTP listening port
    https: {
        privateKey: 'resource/https/server.key', // HTTPS private key
        certificate: 'resource/https/server.crt', // HTTPS cert
        port: 5040, // HTTPS listening port
    },
    cache:  {
        defaultTTL: 60,
        defaultRefetchThreshold: 15
    },
    kvdb: {
        protocol: 'http',
        host: ['127.0.0.1'],
        port: 8098,
        timeout: 10 * 1000
    },
    apis: {
        path: 'apis'
    },
    objs: {
        path: 'schema'
    },
    exceptions: {
        catchAll: true
    }
};
