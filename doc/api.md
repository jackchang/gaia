#GAIA programming guide

GAIA is the spirit successor of Moore framework. Optimized for high volume concurrent data access, GAIA supports:

* Object storage with index based query
* Key-value based cache system with TTL support
* Binary storage with queryable meta data

(to be finished)

* Distributed file system with massive file size support
* Distributed parallel task
* Machine learning packages, including large data set cluster and regression support
* Neural network packages, such as convolutional neural network

##Programming environment

GAIA programs are developed in JavaScript, running in a sandbox'ed node.js environment.

##Use JSON object storage
###Methods: ```GET```, ```PUT```, ```DELETE```, ```SEARCH```, ```PATCH```

GAIA JSON object storage supports a ```PUT``` interface compatible with Moore.

###Define an object schema

Object schema can defined using javascript:

```javascript
module.exports = (core) => core('Namespace.SchemaTest')
    .id((obj) => obj.objectId)
    .shadowClass(false)
    .multiValue(true)
    .searchOrder(['key1', 'key2', 'key3'])
    .watch('key1')
    .watch('key2')
    .watch('key3')
    .onPut((objects) => {
        core('Namespace.Log').put({
            event: 'put',
            count: objects.length
        });    
    });
```

The ```core``` function returns a class definition handle exports following functions:

**Mandatory functions**

These three functions are basic requirements for a class definition and should be called only once per class. ``id()`` and ``multiValue()`` can be ommited for **shadow class**.
 
* ```id(string propertyName || function callback)```: defines an object ID generator function ```callback```. The function will be called with single object as input parameters and must return a string id. If the input parameter is a string, a function ```(obj) => obj[propertyName]``` will be generated.
    
* ```shadowClass(bool onOff)```: turn on/off **shadow class** feature. Please check about **shadow class** in following sections.
    
* ```multiValue(bool onOff)```: turn on/off **multi value** feature. Please check about **multi value** in following sections.

**Index and search related functions**

A class may has zero or one or more index'ed properties. It's highly recommended to define searchOrder for each index as well.

* ```watch(string property)```: specify a ``property`` should be indexed. Sub-properties are supported.

* ```searchOrder(string[] properties)```: when performing multi-condition queries, only single index will be used and objects will be retrieved post-filtered with rest predicates. Thus it's recommended to give hints about which index is preferred. Properties with high cardinality should have higher priority.

* ```searchFilter(function)```: register a filter function when performing a multi-condition query. The function will be called with query conditions as first parameter and single object id as second parameter. The function only needs to return false when an id should be rejected. Returning ``true`` or nothing at all will produce the same result.

**Object validation functions**

* ```require(string property)```: specify a ``property`` must have a defined value.

* ```notNull(string property)```: specify a ``property`` must have a defined value and the value isn't **null**.

* ```notEmpty(string property)```: specify a ``property`` must have a defined value and the value isn't **null** or empty string (``''``).

* ```inRange(string property, number lowerBound, number higherBound)```: specify a ``property`` must have a defined value and the value must between range specified. The bounds are inclusive. One of the bounds can be **null** to perform a one bound check.

* ```validate(string ruleName, function callback)```: register a validation callback function on object ``PUT``. The function will be called with single object as input parameters and must return ``bool`` indidating success or not.

**Event hooks**

It's also useful to add callback functions when objects are put or rejected. For **multi value** classes, a merger can be optionally defined to merge multi objects. Each class should call ``merger`` only once.

* ```onPut(function callback)```: register a callback function on object ``PUT``. The function will be called with an object array as input parameters. The callback function may returns a ``Promise`` object and the actual ``PUT`` request will be performed when all ``Promise`` are resolved. The callback function may returns **null** to if it doesn't need to be waited for.

* ```merger(function callback)```: register a callback function on objects merged into a single record. The function will be called with an object array as input parameters. The function may returns an object array to substitude original objects. ``onPut`` callbacks will be invoked when substitution occurs. The function will only be called if **multi value** is enabled.

* ```onReject(function callback)```: register a callback function when objects are rejected by a validation function. The function will be called with an array of rejections. Each rejection is an array with single object as first element and validation rule name as second element.

**Put or delete objects**

* ```put(object[] objs || object obj)```: function to ```PUT``` object(s) into the database.

* ```put(string objectId, object[] objs || object obj)```: function to ```PUT``` object(s) into the database.

* ```put(string objectId, object index, object[] objs || object obj)```: function to ```PUT``` object(s) into the database.

* ```get(string objectId)```:

* ```get(string objectId, number cacheForSeconds)```:

* ```get(object queryParameters)```: 

* ```get(object queryParameters, number cacheForSeconds)```: 

* ```cache(string key, number value || string value || object value, number ttl)```:

* ```recall(string key, number refetchIfOlder)```:

###Shadow class

Shadow class works as a virtual entry point for ``PUT`` while no object will be actually ``PUT`` into the database. It's often used to generate events. For example:

```javascript
core('Namespace.Shadow.UserAction')
    .shadowClass(true)
    .onPut((objs) => {
        objs.forEach((obj) => {
            switch (obj.action) {
                case 'login': core('Namespace.UserLog').put({
                    user: obj.user,
                    action: 'login',
                    timestamp: Date.now(),
                    server: getServerId()
                }); break;
                case 'logout': core('Namespace.UserLog').put({
                    user: obj.user,
                    action: 'logout',
                    timestamp: Date.now(),
                    server: getServerId()
                }); break;
            }
        });
    });
```

###Multi value class and object lazy merging

By default, an object id can only refer to single object. Each ``PUT`` to the same id will overwrite previous record.

Enabling **multi value** will allow multiple objects stored under single id. Multiple ``PUT`` to the same id will generate multiple records. Records will be consolidated on a ``GET`` request automatically.

By default, multiple objects will be consolidated into an array and stored as a single record. It's possible to customize the merging behavior by registering a ``merger`` function. For example:

```javascript
core('Namespace.SchemaTest').merger((objs) => {
    let group = {};
    objs.forEach((obj) => {
        let g = obj.groupName;
        let old = group[g];
        if ((!old) || (old.timestamp < obj.timestamp)) {
            group[g] = obj; 
        }
    });
    return Object.keys(group).map((g) => group[g]);
});
```

A max of 128 records are allowed per object id before objects are merged.

###Validate objects

Objects can be validated before put into the database. There are several different validation functions can be used.

* ``require('propertyName')``: ``obj.propertyName`` must be defined. 
* ``notnull('propertyName')``: ``obj.propertyName`` can't be null.
* ``notempty('propertyName')``: ``obj.propertyName`` can't be null or empty string.
* ``inRange('propertyName', 10, 100)``: ``obj.propertyName`` must be equal or greater then 10 and equal to or less than 100.
* ``inRange('propertyName', null, 100)``: ``obj.propertyName`` must be equal to or less then 100. 
* ``inRange('propertyName', 10, null)``: ``obj.propertyName`` must be equal to or greater then 10.
* ``validate('check-email-format', (obj) => { return obj.email.indexOf('@') !== -1; })``: perform a custom check with rule name **check-email-format**.

###Index and search

Objects can be indexed by property name:

```javascript
core('Namespace.SchemaTest').watch('propertyName');
```

Sub properties are also supported:

```javascript
core('Namespace.SchemaTest')
    .watch('location.country')
    .watch('location.city');
```

Search orders should specified based on cardinality:

```javascript
core('Namespace.SchemaTest')
    .watch('location.country')
    .watch('location.city')
    .searchOrder(['location.city', 'location.country']);
```

Custom id filter:

```javascript
core('Namespace.SchemaTest')
    .watch('location.country')
    .watch('location.city')
    .searchOrder(['location.city', 'location.country'])
    .searchFilter((query, oid) => {
        let group = query.group;
        if (!group) { return; }
        return oid.split('-')[1] === group;
    })
    .searchFilter((query, oid) => {
        let department = query.department;
        if (!department) { return; }
        return oid.split('-')[0] === department;
    });
```

###Upload an object schema with ```PATCH```

Object schema can be uploaded through a http ```PATCH``` request:

```bash
SERVER_IP="127.0.0.1"
PORT="8080"
SCHEMA_ID="namespace.schematest"
CLASS_NAME="Namespace.SchemaTest"

curl -XPATCH http://${SERVER_IP}:${PORT}/${SCHEMA_ID} -d \
"module.exports = (core) => core('${CLASS_NAME}')\
    .id((obj) => obj.objectId)\
    .multiValue(true)\
    .searchOrder(['property1', 'property2'])\
    .watch('property1')\
    .watch('property2');"
```

The ``SCHEMA_ID`` should be unique across the system. The ``CLASS_NAME`` will be the public entry point for ``PUT``, ``GET``, ``SEARCH`` and ``DELETE`` operations.  It's possible to separate a class schema into multiple parts. It's recommended to define object schema in a single request and put event hooks into separate requests:

```bash
SERVER_IP="127.0.0.1"
PORT="8080"
SCHEMA_ID="namespace.schematest"
CLASS_NAME="Namespace.SchemaTest"

curl -XPATCH http://${SERVER_IP}:${PORT}/${SCHEMA_ID} -d \
"module.exports = (core) => core('${CLASS_NAME}')\
    .id((obj) => obj.objectId)\
    .multiValue(true)\
    .searchOrder(['property1', 'property2'])\
    .watch('property1')\
    .watch('property2');"

SCHEMA_ID="namespace.schematest.log"
curl -XPATCH http://${SERVER_IP}:${PORT}/${SCHEMA_ID} -d \
"module.exports = (core) => core('${CLASS_NAME}')\
    .onPut((objs) => {
        core('namespace.syslog').put({
            time: Date.now(),
            action: 'put',
            class: '${CLASS_NAME}',
            count: objs.length
        });
    });"    
```

###Delete an object schema with ```PATCH```

If the ``PATCH`` request has zero length payload. The schema definition of ``SCHEMA_ID`` will be deleted.

```bash
SERVER_IP="127.0.0.1"
PORT="8080"
SCHEMA_ID="namespace.schematest"
CLASS_NAME="Namespace.SchemaTest"

curl -XPATCH http://${SERVER_IP}:${PORT}/${SCHEMA_ID} -d ''
```

###Upload objects with ```PUT```

```bash
SERVER_IP="127.0.0.1"
PORT="8001"
BUCKET_NAME="myobj"

curl -XPUT http://${SERVER_IP}:${PORT}/${BUCKET_NAME} -d \
"[
    { objectId: 103, property1: 1 },
    { objectId: 104, property1: 13 }
]"
```

###Upload objects without schema defined with ```PUT```

It's recommended to define and upload schema for readability. However, it's still possible to upload objects without defining a schema first. In such case, object id must be supplied:

```bash
SERVER_IP="127.0.0.1"
PORT="8001"
BUCKET_NAME="myobj"
OBJECT_ID="105"

curl -XPUT http://${SERVER_IP}:${PORT}/${BUCKET_NAME}/${OBJECT_ID} -d \
"{ objectId: 103, property1: 1 }"
```

By default **multi value** is disabled. To enable **multi value**, change payload into a JSON object array.

```bash
SERVER_IP="127.0.0.1"
PORT="8001"
BUCKET_NAME="myobj"
OBJECT_ID="105"

curl -XPUT http://${SERVER_IP}:${PORT}/${BUCKET_NAME}/${OBJECT_ID} -d \
"[{ objectId: 103, property1: 1 }]"
```

Index can also be specified using query string

```bash
SERVER_IP="127.0.0.1"
PORT="8001"
BUCKET_NAME="myobj"
OBJECT_ID="105"
INDEX="user=foobar"

curl -XPUT http://${SERVER_IP}:${PORT}/${BUCKET_NAME}/${OBJECT_ID}?${INDEX} -d \
"{ objectId: 103, property1: 1 }"
```

###Retrieve objects with ```GET```

Retrive objects by object id.

```bash
SERVER_IP="127.0.0.1"
PORT="8001"
BUCKET_NAME="myobj"
OBJECT_ID="105"

curl -XGET http://${SERVER_IP}:${PORT}/${BUCKET_NAME}/${OBJECT_ID}
```

Retrive objects with query conditions.

```bash
SERVER_IP="127.0.0.1"
PORT="8001"
BUCKET_NAME="myobj"
CONDITION="user=foo&group=bar"

curl -XGET http://${SERVER_IP}:${PORT}/${BUCKET_NAME}?${INDEX}
```

###Retrieve object list with ```SEARCH```

Sometimes it's useful to retrieve object id list without actually retrieve the data.

```bash
SERVER_IP="127.0.0.1"
PORT="8001"
BUCKET_NAME="myobj"
CONDITION="user=foo&group=bar"

curl -XSEARCH http://${SERVER_IP}:${PORT}/${BUCKET_NAME}?${INDEX}
```

###Delete objects with ```DELETE```

Delete objects by object id.

```bash
SERVER_IP="127.0.0.1"
PORT="8001"
BUCKET_NAME="myobj"
OBJECT_ID="105"

curl -XDELETE http://${SERVER_IP}:${PORT}/${BUCKET_NAME}/${OBJECT_ID}
```


##Use binary storage

###Bucket prefix: ```=```
###Methods: ```GET```, ```PUT```

Binary storage system provides a way to store opaque data type with meta data supports.

###PUT binary objects
To store an object into the system, use the following ```PUT``` request:

```bash
SERVER_IP="127.0.0.1"
PORT="8001"
BUCKET_NAME="myBinaryObjects"
OBJECT_NAME="obj1"
ATTR1="FOO1=BAR1"
ATTR2="FOO2=13"

curl -XPUT --data @~/data.tgz \
    http://${SERVER_IP}:${PORT}/=${BUCKET_NAME}/${OBJECT_NAME}?\
    ${ATTR1}&\
    ${ATTR2}
```

###GET objects with object ID
Objects stored can be retrieved by using the same URL with ```GET``` request:

```bash
SERVER_IP="127.0.0.1"
PORT="8080"
BUCKET_NAME="myBinaryObjects"
OBJECT_NAME="myBinaryObject1"

curl http://${SERVER_IP}:${PORT}/=${BUCKET_NAME}/${OBJECT_NAME}
```

###GET object list with query condition

A list of object names with meta data can be retrieved with ```GET``` request:
**Currently only single condition is supported.**


```bash
SERVER_IP="127.0.0.1"
PORT="8080"
BUCKET_NAME="myBinaryObjects"
CONDITION="FOO1=BAR1"

curl http://${SERVER_IP}:${PORT}/=${BUCKET_NAME}?${CONDITION}
```

Results returned from the query will be a json object looks like:

```json
{
  "status": 200,
  "description": "",
  "data": [
    "myBinaryObject1"
  ]
}
```

The ```data``` property is an array contains object names, which can be used to retrieve the object data.