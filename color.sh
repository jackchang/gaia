Black='\033[0;30m'
DarkGray='\033[1;30m'
DarkGrey='\033[1;30m'
Red='\033[0;31m'
LightRed='\033[1;31m'
Green='\033[0;32m'
DarkGreen='\033[1;32m'
Orange='\033[0;33m'
Yellow='\033[1;33m'
Blue='\033[0;34m'
LightBlue='\033[1;34m'
Purple='\033[0;35m'
LightPurple='\033[1;35m'
Cyan='\033[0;36m'
LightCyan='\033[1;36m'
LightGray='\033[0;37m'
LightGrey='\033[0;37m'
White='\033[1;37m'

F=''
ME=`basename "${0}"`

function foreground {
    printf "${1}"
    F=${1}
}

function reset_console {
    printf "\033[0m"
}

function p {
    printf "${1}${2}${F}\n"
}

function warning {
    p ${Yellow} "${1}"
}

function error {
    p ${Red} "${1}"
}

function message {
    p ${DarkGreen} "${1}"
}

function debug {
    p ${DarkGray} "${1}"
}

function section {
    message "------------------------------------------------------"
    p "${DarkGreen}${ME} - ${Green} ${1}"
    # message "${ME} - ${1}"
    message "------------------------------------------------------"
}


reset_console
